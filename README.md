# The demo
Builds a JAR and publishes it to Artifactory. It then creates a project and image in OpenShift, using a source repository and the JAR published to Artifactory. This image then run a simple WildFly web server.


## The repositories
This demo consists of 5 repositories:

* [demo-docs](https://bitbucket.org/sffortytwoteam/demo-docs) - This documentation
* [openshift-demo](https://bitbucket.org/sffortytwoteam/openshift-demo) - (private) A repository that builds all the necessary containers using `docker-compose`
* [blueprint-demo](https://bitbucket.org/sffortytwoteam/blueprint-demo) - (private) A repository that holds a Xebialabs blueprint
* [simple-web-lib](https://bitbucket.org/sffortytwoteam/simple-web-lib) - A JAR that is built and published to Artifactory
* [simple-web-app](https://bitbucket.org/sffortytwoteam/simple-web-app) - A project used by OpenShift's S2I to produce an image (depends on the previous JAR)


## Supporting Technologies
I used:
* Mac OS 10.14.2
* Docker Desktop 2.0.0.0-mac81
* VirtualBox 5.2.22
* MiniShift 1.28.0

### Notes on Docker Desktop
The default setup uses 4 CPUs. This can cause the Jenkins builds to hang sometimes. Reducing the CPU count to 2 fixes this.

### Notes on MiniShift / OpenShift
This project will work with a separate OpenShift or an instance of MiniShift running on your machine.


## Private key
I have created an SSH key for cloning the private repositories. It is in the email. Save it to a file or your machine.

### Set up the private key
You'll need to configure Git to use the private key. There are a few suggestions here you can try: https://superuser.com/questions/232373/how-to-tell-git-which-private-key-to-use, but one way that works is to add the following lines to `$HOME/.ssh/config`:

```
Host bitbucket.org
     Hostname bitbucket.org
     User git
     IdentityFile </path/to/private_key>
```


## Installing MiniShift
This demo was built with MiniShift on VirtualBox. You can use a different Hypervisor for MiniShift, but take note of its IP so you can enter it when creating the blueprint.

Download and install VirtualBox 5.2.22 here: https://www.virtualbox.org/wiki/Download_Old_Builds_5_2
Download MiniShift v1.28.0 here: https://github.com/minishift/minishift/releases

Configure MiniShift to use VirtualBox:
```
minishift config set vm-driver virtualbox
```

Start MiniShift
```
minishift start --memory 6144
```

If MiniShift times out on first start:
```
minishift stop
minishift start --memory 6144
```

> Note the IP for MiniShift's console and test that you can log in (e.g. https://192.168.99.100:8443/console)
> By default, MiniShift on VitualBox runs on address `192.168.99.100`

Username: `developer`
Password: `developer`

## For OpenShift
Ask an administrator for credentials of a user that can create a project and builds

### Step 1: Clone and run `openshift-demo`
First you'll need to start up the servers and services to run the demon:
* XL Release
* Jenkins
* Artifactory
* Ansible jump station

```
git clone git@bitbucket.org:sffortytwoteam/openshift-demo.git
cd openshift-demo/docker-compose/jeffdemo/
docker-compose up
```

Make sure that `xl-cli` exits with status 0. If it doesn't, rerun it until it exits with status 0.
```
docker-compose up -d xl-cli
```

### Step 2: Test that everything is working
Log into the servers with the following details:

#### XL Release
* URL: `http://localhost:5516`
* Username: `admin`
* Password: `admin`

#### Jenkins
* URL: `http://localhost:8080`
* Username: `admin`
* Password: `admin`

#### Artifactory
* URL: `http://localhost:8081`
* Username: `admin`
* Password: `password`

### Step 2: Apply the blueprint
This step assumes you're running the blueprint as a local file.

```
git clone git@bitbucket.org:sffortytwoteam/blueprint-demo.git
cd blueprint-demo/openshift/
xl blueprint -b jeff-demo
```

* Enter the MiniShift IP or the IP of a running OpenShift server
* The port should always be 8443
* If you're using MiniShift, the default credentials are `developer/developer`
* If you're using OpenShift, enter the credentials of a user that can create projects and builds
* MiniShift / OpenShift will pull the JAR created in `simple-web-lib`
  * If you're using a separate OpenShift, enter the address of your host machine and port 8081
  * If you're running MiniShift, enter the MiniShift IP address, but with the last segment as `1`
    * e.g. If MiniShift is running on `192.168.99.100`, then Artifactory's IP will be `192.168.100.1`

Now apply the blueprint.

```
xl apply -f xebialabs.yaml
```

### Step 3: Run the release in XL Release
* Go back to XL Release (`http://localhost:5516`)
* Browse to _Design_ > _Folders_ > _jeffdemo_ > _Templates_ > _OpenShift demo_
* Click _New release_
* Give the release a name (e.g. `release 1`)
* Click _Create_

In the _Releases_ screen:
* Click _Start Release_, then click _Start_

### Step 4: Browse the finished web page
Once the Release is down, return to MiniShift / OpenShift and the Demo project.
* Click _Overview_
* Expand _simple-web-app_
* Click on the link under _Routes - External Traffic_
* Follow the instructions and add `/hello` to the url


## Known issues
Sometimes OpenShift will hang at the clone phase of a build (see: https://github.com/rtfd/readthedocs.org/issues/2047). This sometimes happens with a fresh MiniShift and can take about 20 minutes before it's available.


## Notes
* I did not add testing
* I did not add a cleanup phase

## Design choices
I chose to use MiniShift because not everyone would have OpenShift when trying out the demo.

MiniShift is using VirtualBox so that it can run on all platforms (Linux, Mac, Windows).

I chose to put Jenkins and Artifactory in Docker because these technologies don't belong in OpenShift.
